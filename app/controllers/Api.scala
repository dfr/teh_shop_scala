package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.data.Form
import play.api.data.Forms._
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import javax.imageio.ImageIO
import scala.util.{Try, Success, Failure}
import com.mongodb.MongoException
import org.imgscalr.Scalr
import org.mindrot.jbcrypt.BCrypt

import models._
import utils.ValidationConstraints._

object Api extends Controller with Secured {

    // TODO: move to config
    val UPLOAD_DIR = "public/upload"
    val UPLOAD_THUMB_DIR = "public/upload/thumbs"

   
    //
    // ***** Categories *****
    //
    val categoryForm = Form(
        mapping(
            "id" -> default(text, "0"),
            "title" -> nonEmptyText,
            "path" -> ignored(""),
            "parent" -> text,
            "description" -> default(text, "")
        )(Category.apply)(Category.unapply)
    )


    def categories = Authenticated { username =>
        val items = Category asJson Category.find()
        Ok(items).as("application/json")
    }
    def saveCategory(id: String) = Authenticated(parse.json) { request =>
        categoryForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            catObj => {
                Logger.info(catObj.toString)
                val newcat = Category.save(catObj)
                Ok(Category asJson newcat).as("application/json")
            }
        )
    }
    def deleteCategory(id: String) = Authenticated {
        val r = Category.remove(id)
        Ok(Json.obj("removed" -> r)).as("application/json")
    }


    //
    // ***** Products ******
    //
    val productAttrMapping =
        mapping(
            "name" -> nonEmptyText,
            "value" -> nonEmptyText,
            "searchType" -> se.radley.plugin.enumeration.form.enum(SearchType)
        )(ProductAttr.apply)(ProductAttr.unapply)

    val productForm = Form(
        mapping(
            "id" -> default(text, "0"),
            "title" -> nonEmptyText,
            "category" -> nonEmptyText,
            "description" -> default(text, ""),
            "images" -> list(text),
            "attrs" -> list(productAttrMapping)
        )(Product.apply)(Product.unapply)
    )

    def products = Action { request =>
        val limit = request.getQueryString("limit").getOrElse("10").toInt
        val offset = request.getQueryString("offset").getOrElse("0").toInt
        val category = request.getQueryString("category").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("category" -> category, "keyword" -> keyword)
        val items = Product asJson Product.find(limit=limit, offset=offset, filter=filter)
        Ok(items).as("application/json")
    }

    def countProducts = Action { request =>
        val category = request.getQueryString("category").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("category" -> category, "keyword" -> keyword)
        Ok(Product.count(filter).toString).as("text/plain")
    }

    def saveProduct(id: String) = Action(parse.json) { request =>
        productForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                val newitem = Product.save(itemObj)
                Ok(Product asJson newitem).as("application/json")
            }
        )
    }
    def deleteProduct(id: String) = Action {
        val r = Product.load(id) match {
            case None => 0
            case Some(itemObj) => {
                val r = Product.remove(itemObj.id)
                if(r > 0) itemObj.images.foreach { removeFileAndThumbnail(_) }
                r
            }
        }
        Ok(Json.obj("removed" -> r)).as("application/json")
    }
    def loadProduct(id: String) = Action {
        Product.load(id) match {
            case None => NotFound("")
            case Some(itemObj) => Ok(Product asJson itemObj).as("application/json")
        }
    }

    def uploadProductFile = Action(parse.multipartFormData) { request =>
        val files : Seq[String] = request.body.files.map { photo =>
            val ext = ExtByContentType(photo.contentType.get)
            if(ext != "") {
                val path = Files.createTempFile(Paths.get(UPLOAD_DIR), "img", s".$ext")
                val filename = path.getFileName.toString
                val orImg = ImageIO.read(photo.ref.file)
                val rsImg = Scalr.resize(orImg, 150)
                ImageIO.write(rsImg, ext, Paths.get(UPLOAD_THUMB_DIR,filename).toFile)
                photo.ref.moveTo(path.toFile, true)
                Some(filename)
            }
            else {
                None
            }
        }.flatten
        Ok(Json.obj("files" -> files))
    }

    def removeProductFile(filename: String) = Action {
        val fnameOk = filename matches """img\d+\.[a-z]+"""
        if(fnameOk) removeFileAndThumbnail(filename)
        Ok(Json.obj("ok" -> fnameOk)).as("application/json")
    }

    def productAttrs(categoryId: Option[String]) = Action {
        val filter = categoryId.map { c =>  List("categoryId" -> c) }.getOrElse(List())
        val attrs = AttrIndex.find(filter)
        Ok(Json toJson attrs)
    }

    def removeFileAndThumbnail(filename: String) = {
        val f1 = Paths.get(UPLOAD_DIR, filename)
        val f2 = Paths.get(UPLOAD_THUMB_DIR, filename)
        Seq(f1,f2).foreach(f => if(Files.isRegularFile(f)) Files.delete(f))
    }

    def ExtByContentType(file: String) = {
        file match {
            case "image/jpeg" => "jpg"
            case "image/png" => "png"
            case "image/gif" => "gif"
            case _ => ""
        }
    }

    //
    // ***** Articles ******
    //
    val articleForm = Form(
        mapping(
            "id" -> default(text, "0"),
            "title" -> nonEmptyText,
            "tags" -> list(
                mapping(
                    "name" -> nonEmptyText
                )(ArticleTag.apply)(ArticleTag.unapply)
            ),
            "text" -> default(text, ""),
            "added" -> ignored(new java.util.Date)
        )(Article.apply)(Article.unapply)
    )

    def articles = Action { request =>
        val limit = request.getQueryString("limit").getOrElse("10").toInt
        val offset = request.getQueryString("offset").getOrElse("0").toInt
        val tags = request.getQueryString("tags").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("tags" -> tags, "keyword" -> keyword)
        val items = Article asJson Article.find(limit=limit, offset=offset, filter=filter)
        Ok(items).as("application/json")
    }

    def countArticles = Action { request =>
        val tags = request.getQueryString("tags").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("tags" -> tags, "keyword" -> keyword)
        Ok(Article.count(filter).toString).as("text/plain")
    }

    def saveArticle(id: String) = Action(parse.json) { request =>
        articleForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                val newitem = Article.save(itemObj)
                Ok(Article asJson newitem).as("application/json")
            }
        )
    }
    def deleteArticle(id: String) = Action {
        val r = Article.remove(id)
        Ok(Json.obj("removed" -> r)).as("application/json")
    }
    def loadArticle(id: String) = Action {
        Article.load(id) match {
            case None => NotFound("")
            case Some(itemObj) => Ok(Article asJson itemObj).as("application/json")
        }
    }

    def articleTags() = Action {
        val tags = ArticleTag.find()
        Ok(Json toJson tags)
    }

    //
    // ***** Users ******
    //
    val userForm = Form(
        mapping(
            "id" -> default(text, "0"),
            "login" -> nonEmptyText,
            "email" -> email,
            "name" -> default(text, ""),
            "password" -> text,
            "groups" -> list(
                mapping(
                    "name" -> nonEmptyText
                )(UserGroup.apply)(UserGroup.unapply)
            ),
            "status" -> se.radley.plugin.enumeration.form.enum(UserStatus),
            "added" -> ignored(new java.util.Date)
        )(User.apply)(User.unapply).verifying(
            userPasswordCheckConstraint,
            uniqueLoginFieldConstraint,
            uniqueEmailFieldConstraint)
    )

    def users = Action { request =>
        val limit = request.getQueryString("limit").getOrElse("10").toInt
        val offset = request.getQueryString("offset").getOrElse("0").toInt
        val groups = request.getQueryString("groups").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("groups" -> groups, "keyword" -> keyword)
        val items = User asJson User.find(limit=limit, offset=offset, filter=filter)
        Ok(items).as("application/json")
    }

    def countUsers = Action { request =>
        val groups = request.getQueryString("groups").getOrElse("")
        val keyword = request.getQueryString("keyword").getOrElse("")
        var filter = List("groups" -> groups, "keyword" -> keyword)
        Ok(User.count(filter).toString).as("text/plain")
    }

    def saveUser() = Action(parse.json) { request =>
        userForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                // if key is empty then try to extract it from message
                                    //key = if(e.key.nonEmpty) e.key else Option(e.message).flatMap( _.split("\\.").lastOption ).getOrElse(""))
                val jsonErrs : Seq[JsObject] = for( e <- errForm.errors;  key = Option(e.key).filter(_.nonEmpty).getOrElse("form") )
                        yield Json.obj("property" -> key,  "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                val newitemObj = if(itemObj.password.isEmpty) itemObj
                    else itemObj.copy(password = BCrypt.hashpw(itemObj.password, BCrypt.gensalt()))
                Try(User.save(newitemObj)).map((u: User) => Ok(User asJson u).as("application/json"))
                    .recover {
                        case e : MongoException.DuplicateKey =>
                            BadRequest(Json.obj("errors" -> Seq(Json.obj("property" -> "login", "message" -> "error.exists"))))
                    }.get
            }
        )
    }
    def deleteUser(id: String) = Action {
        val r = User.remove(id)
        Ok(Json.obj("removed" -> r)).as("application/json")
    }
    def loadUser(id: String) = Action {
        User.load(id) match {
            case None => NotFound("")
            case Some(itemObj) => Ok(User asJson itemObj.copy(password = "")).as("application/json")
        }
    }

    //
    // ***** UserGroups ******
    //
    val userGroupForm = Form(
        mapping(
            "name" -> nonEmptyText
        )(UserGroup.apply)(UserGroup.unapply)
    )

    def userGroups = Action { request =>
        val items = UserGroup asJson UserGroup.find()
        Ok(items).as("application/json")
    }

    def countUserGroups = Action { request =>
        Ok(UserGroup.count().toString).as("text/plain")
    }

    def saveUserGroup(id: String) = Action(parse.json) { request =>
        userGroupForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                val newitem = UserGroup.save(itemObj)
                Ok(UserGroup asJson newitem).as("application/json")
            }
        )
    }
    def deleteUserGroup(id: String) = Action {
        val r = UserGroup.remove(id)
        Ok(Json.obj("removed" -> r)).as("application/json")
    }

    //
    // ***** AttrIndex ******
    //
    val attrIndexForm = Form(
        mapping(
            "id" -> default(text, "0"),
            "name" -> nonEmptyText,
            "categoryId" -> ignored(""),
            "searchType" -> se.radley.plugin.enumeration.form.enum(SearchType)
        )(AttrIndex.apply)(AttrIndex.unapply)
    )

    def attrIndex = Action { request =>
        val keyword = request.getQueryString("keyword").getOrElse("")
        val searchtype = request.getQueryString("searchtype").getOrElse("")
        var filter = List("keyword" -> keyword, "searchType" -> searchtype)
        val items = AttrIndex asJson AttrIndex.find(filter)
        Ok(items).as("application/json")
    }
    
    def loadAttrIndex(id: String) = Action {
        AttrIndex.load(id) match {
            case None => NotFound("")
            case Some(itemObj) => Ok(AttrIndex asJson itemObj).as("application/json")
        }
    }

    def countAttrIndex = Action { request =>
        Ok(AttrIndex.count().toString).as("text/plain")
    }

    def saveAttrIndex() = Action(parse.json) { request =>
        attrIndexForm.bind(request.body).fold(
            errForm => {
                // { errors: [{message: '...', property: '...'}, ...  ] }
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                val newitem = AttrIndex.save(itemObj)
                Ok(AttrIndex asJson newitem).as("application/json")
            }
        )
    }
    def deleteAttrIndex(id: String) = Action {
        val r = AttrIndex.remove(id)
        Ok(Json.obj("removed" -> r)).as("application/json")
    }

}


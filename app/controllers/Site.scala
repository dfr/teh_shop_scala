package controllers

import scala.concurrent.Future
import play.api._
import play.api.mvc._
import play.api.mvc.Security
import play.api.libs.json._
import play.api.data.Form
import play.api.data.Forms._

import models._

object Site extends Controller {

    def index = Action { implicit request =>
        val categories = Category.find()
        Ok(views.html.index(categories))
    }

    def listProducts(categoryId: String) = Action { implicit request =>
        val categories = Category.find()
        val products = Product.find(filter=List("category" -> categoryId))
        Ok(views.html.listProducts(categories, products))
    }

    def viewProduct(id: String) = Action { implicit request =>
        val categories = Category.find()
        Product.load(id).map { p => Ok(views.html.viewProduct(p, categories)) }.getOrElse { NotFound("Product not found") }
    }

}

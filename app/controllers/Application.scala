package controllers

import scala.concurrent.Future
import play.api._
import play.api.mvc._
import play.api.mvc.Security
import play.api.libs.json._
import play.api.data.Form
import play.api.data.Forms._
import org.mindrot.jbcrypt.BCrypt

import models._

object Application extends Controller {

    def index = Action { implicit request =>
        val categories = Category.find()
        Ok(views.html.index(categories))
    }

    def untrail(path: String) = Action { 
        MovedPermanently("/" + path)
    }

    // Auth

    val loginForm = Form(
        tuple(
            "email" -> text,
            "password" -> text
            ) verifying ("Invalid login or password", result => result match {
                case (l, p) =>
                    User.findByLogin(l).map(u => BCrypt.checkpw(p, u.password)).getOrElse(false)
            })
        )


    def authenticate = Action(parse.json) { request =>
        loginForm.bind(request.body).fold(
            errForm => {
                val jsonErrs = for(e <- errForm.errors) yield Json.obj("property" -> e.key, "message" -> e.message)
                BadRequest(Json.obj("errors" -> jsonErrs))
            },
            itemObj => {
                Ok(Json.obj("ok" -> routes.Site.index.url)).as("application/json").withSession(Security.username -> itemObj._1)
            }
        )
    }


    def logout = Action { req =>
        Redirect(routes.Site.index.url).withNewSession.flashing(
            "success" -> "You've been logged out"
        )
    }
}

trait Secured {

    class AuthRequest[A](val username: String, request: Request[A]) extends WrappedRequest[A](request)

    object Authenticated extends ActionBuilder[AuthRequest] {
        def invokeBlock[A](request: Request[A], block: (AuthRequest[A]) => Future[SimpleResult]) = {
            request.session.get(Security.username).map { username =>
                block(new AuthRequest(username, request))
            } getOrElse {
                //Future.successful(Results.Forbidden)
                Future.successful(Results.Redirect(routes.Site.index))
            }
        }
    }
  
    def IsMemberOf(group : String) = new ActionBuilder[AuthRequest] {
      def invokeBlock[A](request: Request[A], block: (AuthRequest[A]) => Future[SimpleResult]) = {
          request match {
              case r : AuthRequest[A] =>
                  User.load(r.username).map { user =>
                    if(user.groups.contains(group))
                        block(r)
                    else Future.successful(Results.Forbidden)
                  } getOrElse { Future.successful(Results.Forbidden) }
              case _ => Future.successful(Results.InternalServerError)
          }
      }

      override def composeAction[A](action: Action[A]): Action[A] = Authenticated.async(action.parser) { request => action(request) }
    }
  

}


package controllers

import play.api._
import play.api.mvc._
import models._

object Admin extends Controller with Secured {

    def index = Authenticated { implicit request =>
        Ok(views.html.admin.index(UserStatus.values, SearchType.values))
    }

}

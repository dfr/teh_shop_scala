package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger
import utils._

case class Product(
  val id: String = "0",
  val title: String,
  val category: String,
  val description: String,
  val images: List[String],
  val attrs: List[ProductAttr]
)

case class ProductAttr(val name: String, val value: String, val searchType: SearchType.Value)

object ProductAttr {
  implicit val productWrites = Json.writes[ProductAttr]
  implicit val productReads = Json.reads[ProductAttr]
}


/**
 * Product object allows to operate with news in database. Companion object for Product class
 */
object Product {

  private val col: MongoCollection = Database.collection("products")

  /**
   * Method to save product
   * @param filled Product object
   * @return product with id added if insert was performed
   */
  def save(item: Product) : Product = {
    val toInsert = MongoDBObject(
      "title" -> item.title,
      "description" -> item.description,
      "category" -> item.category,
      "images" -> item.images,
      "attrs" -> item.attrs.map(a => MongoDBObject("name" -> a.name, "value" -> a.value, "searchType" -> a.searchType.toString))
    )
    // update attrs index
    item.attrs.foreach { attr => AttrIndex.save(AttrIndex("0", attr.name, item.category, attr.searchType)) }
    // save or update  Product
    if(item.id != "0") {
      col.update(MongoDBObject("_id" -> new ObjectId(item.id)), toInsert)
      item
    }
    else {
      col.insert(toInsert)
      item.copy(id = toInsert.as[ObjectId]("_id").toString)
    }
  }

  /**
   * Remove products from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
    col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }

  /**
   * load single product from DB
   */
  def load(id: String): Option[Product] = {
    col.findOneByID(new ObjectId(id)).map(fromMongo(_))
  }

  /**
   * Get products from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   */
  def find(limit: Int = 10, offset: Int = 0, filter: Seq[(String, String)]): Array[Product] = {
    val sort = MongoDBObject("path" -> 1)
    col.find(mkMongoFilter(filter)).limit(limit).skip(offset).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * Count total number of products int DB
   */
  def count(filter: Seq[(String, String)]): Long = {
    col.count(mkMongoFilter(filter))
  }

  /**
   * Play Magic
   */
  implicit val productWrites = Json.writes[Product]
  implicit val productReads = Json.reads[Product]

  /**
   * Converts array of model objects to json
   * @param src Array of Product instances
   * @return JSON string
   */
  def asJson(src: Array[Product]) = Json.stringify(Json.toJson(src))
  def asJson(src: Product) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): Product = {
    val images : Option[List[String]]  = o.getAs[MongoDBList]("images") map { _.toList.collect { case x: String => x } }
    val attrs : Option[List[ProductAttr]]  = o.getAs[MongoDBList]("attrs") map { attrs: MongoDBList =>
      attrs.toList.collect { case o: BasicDBObject => {
        ProductAttr(o.as[String]("name"), o.as[String]("value"), SearchType.withName(o.as[String]("searchType")))
      }}
    }
    new Product(
      id = o.as[ObjectId]("_id").toString,
      title = o.as[String]("title"),
      category = o.as[String]("category"),
      description = o.getAs[String]("description") getOrElse "",
      images = images getOrElse List.empty,
      attrs = attrs getOrElse List.empty
    )
  }

  def mkMongoFilter(filter: Seq[(String, String)]): DBObject = {
    val mfilter = MongoDBObject()
    filter.filter(!_._2.isEmpty).foreach { p =>
      p._1 match {
        case "category" => mfilter += p
        case "keyword" => {
          mfilter ++= $or("title" -> p._2.r, "description" -> p._2.r)
        }
      }
    }
    mfilter
  }

}


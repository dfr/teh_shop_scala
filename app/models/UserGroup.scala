package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger

case class UserGroup(
    val name: String
)


object UserGroup {

  private val col: MongoCollection = Database.collection("usergroups")

  /**
   * Method to save usergroup
   * @param filled UserGroup object
   * @return same usergroup object with id set (if insert)
   */
  def save(item: UserGroup) : UserGroup = {
    val toIns = MongoDBObject("_id" -> item.name)
    col.update(MongoDBObject("_id" -> item.name), toIns, upsert=true)
    item
  }

  /**
   * Remove usergroups from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
      col.remove(MongoDBObject("_id" -> id)).getN
  }

  /**
   * load single group from DB
   */
  def load(id: String): Option[UserGroup] = {
    col.findOneByID(id).map(fromMongo(_))
  }

  /**
   * Get usergroups from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   */
  def find(limit: Int = 1000, offset: Int = 0): Array[UserGroup] = {
      val sort = MongoDBObject("_id" -> 1)
      col.find().limit(limit).skip(offset).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * Count total number of usergroups int DB
   */
  def count(): Long = col.count()

  /**
   * 
   */
  implicit val userGroupWrites = Json.writes[UserGroup]
  implicit val userGroupReads = Json.reads[UserGroup]

  /**
   * Converts array of model objects to json
   * @param src Array of UserGroup instances
   * @return JSON string
   */
  def asJson(src: Array[UserGroup]) = Json.stringify(Json.toJson(src))
  def asJson(src: UserGroup) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): UserGroup = {
    new UserGroup(name = o.as[String]("_id"))
  }

}


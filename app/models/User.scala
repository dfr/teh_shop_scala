package models

import com.mongodb.casbah.Imports._
import play.api.libs.json._
import play.api.Logger
import java.util.Date
import utils._

object UserStatus extends Enumeration {
    val Unconfirmed = Value("unconfirmed")
    val Confirmed = Value("confirmed")
    val Blocked = Value("blocked")

    //implicit val enumReads: Reads[UserStatus.Value] = se.radley.plugin.enumeration.json.enum(UserStatus)
    implicit val enumReads: Reads[UserStatus.Value] = EnumUtils.enumReads(UserStatus)
    implicit def enumWrites: Writes[UserStatus.Value] = EnumUtils.enumWrites
    //implicit val enumReads = EnumUtils.enumReads(UserStatus)
    //implicit def enumWrites = EnumUtils.enumWrites
}

case class User(
    val id: String = "0",
    val login: String,
    val email: String,
    val name: String,
    val password: String,
    val groups: List[UserGroup],
    val status: UserStatus.Value,
    val added: Date = new Date()
)

object User {

  private val col: MongoCollection = Database.collection("users")

  /**
   * Method to save user
   * @param filled User object
   * @return same user object with id set (if insert)
   */
  def save(item: User) : User = {
      val toInsert = MongoDBObject(
        "login" -> item.login,
        "email" -> item.email,
        "name" -> item.name,
        "groups" -> item.groups.map(_.name),
        "status" -> item.status.toString,
        "added" -> item.added)
    if(item.password.nonEmpty) toInsert += ("password" -> item.password)
    // save or update  User
    if(item.id != "0") {
        col.update(MongoDBObject("_id" -> new ObjectId(item.id)), MongoDBObject("$set" -> toInsert))
        item
    }
    else {
        col.insert(toInsert)
        item.copy(id = toInsert.as[ObjectId]("_id").toString)
    }
  }

  /**
   * Remove users from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
      col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }

  /**
   * load single user from DB
   */
  def load(id: String): Option[User] = {
    col.findOneByID(new ObjectId(id)).map(fromMongo(_))
  }

  /**
   * Get users from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   */
  def find(limit: Int = 10, offset: Int = 0, filter: Seq[(String, String)]): Array[User] = {
      val sort = MongoDBObject("login" -> 1)
      col.find(mkMongoFilter(filter)).limit(limit).skip(offset).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * Authenticate
   * @param login string
   * @param password string
   */
  def findByLogin(login: String): Option[User] = {
      val f1 = MongoDBObject("login" -> login)
      val f2 = MongoDBObject("email" -> login)
      col.find($or(f1, f2)).map(fromMongo(_)).toList.headOption
  }

  /**
   * Count total number of users int DB
   */
  def count(filter: Seq[(String, String)]): Long = {
      col.count(mkMongoFilter(filter))
  }

  /**
   * Count total number of users int DB
   */
  def count(): Long = {
      col.count()
  }

  /**
   * make indexes
   */
  def mkIndexes(): Unit = {
      col.ensureIndex(MongoDBObject("login" -> 1), MongoDBObject("unique" -> true))
      col.ensureIndex(MongoDBObject("email" -> 1), MongoDBObject("unique" -> true))
  }

  /**
   * 
   */
  implicit val userWrites = Json.writes[User]
  implicit val userReads = Json.reads[User]

  /**
   * Converts array of model objects to json
   * @param src Array of User instances
   * @return JSON string
   */
  def asJson(src: Array[User]) = Json.stringify(Json.toJson(src))
  def asJson(src: User) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): User = {
    val groups : Option[List[UserGroup]]  = o.getAs[MongoDBList]("groups") map { _.toList.collect { case x: String => UserGroup(x) } }
    User(
        id = o.as[ObjectId]("_id").toString,
        login = o.as[String]("login"),
        email = o.as[String]("email"),
        name = o.as[String]("name"),
        password = o.as[String]("password"),
        groups = groups getOrElse List.empty,
        status = o.getAs[String]("status") map { UserStatus.withName(_) } getOrElse UserStatus.Unconfirmed,
        added = o.as[Date]("added")
    )
  }

  def mkMongoFilter(filter: Seq[(String, String)]): DBObject = {
      val mfilter = MongoDBObject()
      filter.filter(!_._2.isEmpty).foreach { p =>
        p._1 match {
            case "keyword" => mfilter ++= $or("login" -> p._2.r, "email" -> p._2.r, "name" -> p._2.r)
            case _ => mfilter += p
        }
      }
      mfilter
  }

}


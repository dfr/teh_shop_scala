package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger
import java.util.Date

case class Article(
    val id: String = "0",
    val title: String,
    val tags: List[ArticleTag],
    val text: String,
    val added: Date = new Date()
)


object Article {

  private val col: MongoCollection = Database.collection("articles")

  /**
   * Method to save article
   * @param filled Article object
   * @return same article object with id set (if insert)
   */
  def save(item: Article) : Article = {
    val toIns = MongoDBObject("title" -> item.title, "tags" -> item.tags.map(_.name), "text" -> item.text, "added" -> item.added)
    // update tags index
    item.tags.foreach { attr => ArticleTag.save(ArticleTag(attr.name)) }
    // save or update  Article
    if(item.id != "0") {
        col.update(MongoDBObject("_id" -> new ObjectId(item.id)), toIns)
        item
    }
    else {
        col.insert(toIns)
        item.copy(id = toIns.as[ObjectId]("_id").toString)
    }
  }

  /**
   * Remove articles from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
      col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }

  /**
   * load single article from DB
   */
  def load(id: String): Option[Article] = {
    col.findOneByID(new ObjectId(id)).map(fromMongo(_))
  }

  /**
   * Get articles from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   */
  def find(limit: Int = 10, offset: Int = 0, filter: Seq[(String, String)]): Array[Article] = {
      val sort = MongoDBObject("title" -> 1)
      col.find(mkMongoFilter(filter)).limit(limit).skip(offset).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * Count total number of articles int DB
   */
  def count(filter: Seq[(String, String)]): Long = {
      col.count(mkMongoFilter(filter))
  }

  /**
   * Count total number of articles int DB
   */
  def count(): Long = {
      col.count()
  }

  /**
   * 
   */
  implicit val articleWrites = Json.writes[Article]
  implicit val articleReads = Json.reads[Article]

  /**
   * Converts array of model objects to json
   * @param src Array of Article instances
   * @return JSON string
   */
  def asJson(src: Array[Article]) = Json.stringify(Json.toJson(src))
  def asJson(src: Article) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): Article = {
    val tags : Option[List[ArticleTag]]  = o.getAs[MongoDBList]("tags") map { _.toList.collect { case x: String => ArticleTag(x) } }
    new Article(
        id = o.as[ObjectId]("_id").toString,
        title = o.as[String]("title"),
        tags = tags getOrElse List.empty,
        text = o.getAs[String]("text") getOrElse "",
        added = o.as[Date]("added")
    )
  }

  def mkMongoFilter(filter: Seq[(String, String)]): DBObject = {
      val mfilter = MongoDBObject()
      filter.filter(!_._2.isEmpty).foreach { p =>
        p._1 match {
            case "tags" => mfilter += p
            case "keyword" => {
                mfilter ++= $or("title" -> p._2.r, "text" -> p._2.r)
            }
        }
      }
      mfilter
  }

}


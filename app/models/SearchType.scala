package models

import play.api.libs.json._
import utils._

object SearchType extends Enumeration {
  val Checkbox = Value("checkbox")
  val Select = Value("select")
  val Text = Value("text")
  val None = Value("none")

  implicit val enumReads: Reads[SearchType.Value] = EnumUtils.enumReads(SearchType)
  implicit def enumWrites: Writes[SearchType.Value] = EnumUtils.enumWrites
}


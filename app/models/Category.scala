package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger

case class Category(val id: String = "0", val title: String, val path: String, val parent: String, val description: String) {
    def depth : Int = path.split(',').size
}
//case class Category(val id: String = "0", val title: String, val path: String, val parent: Map[String,String], val description: String)

/**
 * Category object allows to operate with news in database. Companion object for Category class
 */
object Category {

  private val col: MongoCollection = Database.collection("categories")

  /**
   * Method to save category
   * @param filled Category object
   * @return category with id added if insert was performed
   */
  def save(item: Category) : Category = {
    val parentCat = if(item.parent.isEmpty) None else col.findOneByID(new ObjectId(item.parent)).map(fromMongo(_))
    val newpath = parentCat.map(p => p.path).getOrElse(",")
    val toInsert = MongoDBObject("title" -> item.title, "description" -> item.description)
    if(item.id != "0") {
        toInsert += ("path" -> (newpath + item.id + ","))
        val r = col.update(MongoDBObject("_id" -> new ObjectId(item.id)), toInsert)
        // update children paths
        col.find(MongoDBObject("path" -> s",${item.id},".r)).foreach { i =>
            val childOldPath = i.as[String]("path")
            val childPath = childOldPath.replaceFirst(s".*,${item.id},", s"${newpath}${item.id},")
            col.update(MongoDBObject("_id" -> i.as[ObjectId]("_id")), $set("path" -> childPath))
        }
        item
    }
    else {
        col.insert(toInsert)
        val newId = toInsert.as[ObjectId]("_id")
        col.update(MongoDBObject("_id" -> newId), $set("path" -> (newpath + newId + ',') ))
        item.copy(id = newId.toString)
    }
  }

  /**
   * Remove categories from DB
   * @param filter filter for remove() method
   * @return 
   */
  def remove(id: String): Int = {
      // update children
      col.find(MongoDBObject("path" -> s",${id},".r)).foreach { i =>
        val childOldPath = i.as[String]("path")
        val childPath = childOldPath.replaceFirst(s",${id},", s",")
        col.update(MongoDBObject("_id" -> i.as[ObjectId]("_id")), $set("path" -> (if(childPath == ',') "" else childPath)))
      }
      // remove item
      col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }

  /**
   * load single category from DB
   */
  def load(id: String): Option[Category] = {
    col.findOneByID(new ObjectId(id)).map(fromMongo(_))
  }

  /**
   * Get categories from DB
   * @param filter filter for find() method
   * @param sort object for sorting. by default sorts by pubDate
   * @return
   */
  def find(filter: MongoDBObject = MongoDBObject(), sort: MongoDBObject = MongoDBObject("path" -> 1)): Array[Category] = {
    try {
      col.find(filter).sort(sort).map(fromMongo(_)).toArray
    } catch {
      case e: MongoException => throw e
    }
  }

  /**
   * Play Magic
   * @return
   */
  implicit def categoryWrites = Json.writes[Category]
  implicit val categoryReads = Json.reads[Category]
  /*
  implicit val categoryReads = new Json.Reads[Category] {
      def reads(js: JsValue): Category = {
          Category(
              (js \ "title").as[String],
              (js \ "parent").as[String],
              (js \ "weight").as[Float]
              )
    }
  }*/

  /**
   * Converts array of news to json
   * @param src Array of Category instances
   * @return JSON string
   */
  def asJson(src: Array[Category]) = Json.stringify(Json.toJson(src))
  def asJson(src: Category) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): Category = {
      val path = o.as[String]("path")
      val parent = path.split(",").filter { ! _.isEmpty }.lastOption.getOrElse("")
      new Category(
        id = o.as[ObjectId]("_id").toString,
        title = o.as[String]("title"),
        path = path,
        parent = parent,
        description = o.getAs[String]("description") getOrElse (""))
  }

}

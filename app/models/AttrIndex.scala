package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger

case class AttrIndex(
  val id: String = "0",
  val name: String,
  val categoryId: String,
  val searchType: SearchType.Value
)


object AttrIndex {

  private val col: MongoCollection = Database.collection("attrindex")
  private val col_products: MongoCollection = Database.collection("products")

  /**
   * Method to save attrindex
   * @param filled AttrIndex object
   * @return same attrindex object
   */
  def save(item: AttrIndex) : AttrIndex = {
    val existing = (if(item.id != "0") col.findOneByID(new ObjectId(item.id)) else col.findOne(MongoDBObject("name" -> item.name))) map fromMongo
    val newId = if(existing.isDefined) existing.get.id else item.id

    val toIns = MongoDBObject("name" -> item.name, "searchType" -> item.searchType.toString)
    if(item.categoryId != "") toIns += ("categoryId" -> item.categoryId)
    col.update(MongoDBObject("_id" -> newId), toIns, upsert=true)

    // Update attr name and status in all products
    // if(item.id != "0" && existing.isDefined && (item.name != existing.get.name || item.searchType != existing.get.searchType)) {
    //   val update = $set("attr.name" -> item.name)
    //   val result = coll.update( MongoDBObject("attrs.name" -> existing.get.name), update )
    // }
    
    item
  }

  /**
   * Remove attrindexs from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
      col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }


  /**
   * Get attrindexes from DB
   */
  def find(filter: Seq[(String, String)]): Array[AttrIndex] = {
      val sort = MongoDBObject("name" -> 1)
      col.find(mkMongoFilter(filter)).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * load single user from DB
   */
  def load(id: String): Option[AttrIndex] = {
    col.findOneByID(new ObjectId(id)).map(fromMongo(_))
  }

  /**
   * Count total number of attrindexs int DB
   */
  def count(): Long = {
      col.count()
  }

  /**
   * make indexes
   */
  def mkIndexes(): Unit = {
      col.ensureIndex(MongoDBObject("name" -> 1), MongoDBObject("unique" -> true))
  }

  /**
   * 
   */
  implicit val attrindexWrites = Json.writes[AttrIndex]
  implicit val attrindexReads = Json.reads[AttrIndex]

  /**
   * Converts array of model objects to json
   * @param src Array of AttrIndex instances
   * @return JSON string
   */
  def asJson(src: Array[AttrIndex]) = Json.stringify(Json.toJson(src))
  def asJson(src: AttrIndex) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): AttrIndex = {
    new AttrIndex(
        id = o.as[ObjectId]("_id").toString,
        name = o.as[String]("name"),
        categoryId = o.getAs[String]("categoryId") getOrElse "",
        searchType = o.getAs[String]("searchType") map { SearchType.withName(_) } getOrElse SearchType.None
    )
  }

  def mkMongoFilter(filter: Seq[(String, String)]): DBObject = {
    val mfilter = MongoDBObject()
    filter.filter(!_._2.isEmpty).foreach { p =>
      p._1 match {
        case "searchType" => mfilter += p
        case "categoryId" => mfilter += p
        case "keyword" => mfilter += ("name" -> p._2.r)
      }
    }
    mfilter
  }
}


package models

import com.mongodb.casbah.Imports._
import play.api.libs.json.Json
import play.api.Logger

case class ArticleTag(
    val name: String
)


object ArticleTag {

  private val col: MongoCollection = Database.collection("tags")

  /**
   * Method to save tag
   * @param filled ArticleTag object
   * @return same object
   */
  def save(item: ArticleTag) : ArticleTag = {
    val toIns = MongoDBObject("_id" -> item.name)
    col.update(MongoDBObject("_id" -> item.name), toIns, upsert=true)
    item
  }

  /**
   * Remove tags from DB
   * @param filter filter for remove() method
   */
  def remove(id: String): Int = {
      col.remove(MongoDBObject("_id" -> new ObjectId(id))).getN
  }


  /**
   * Get tags from DB
   */
  def find(name: Option[String] = None): Array[ArticleTag] = {
      val sort = MongoDBObject("_id" -> 1)
      val filter = MongoDBObject()
      name foreach { v =>  filter += ("name" -> v) }
      col.find(filter).sort(sort).map(fromMongo(_)).toArray
  }

  /**
   * Count total number of tags int DB
   */
  def count(): Long = {
      col.count()
  }

  /**
   * 
   */
  implicit val tagWrites = Json.writes[ArticleTag]
  implicit val tagReads = Json.reads[ArticleTag]

  /**
   * Converts array of model objects to json
   * @param src Array of ArticleTag instances
   * @return JSON string
   */
  def asJson(src: Array[ArticleTag]) = Json.stringify(Json.toJson(src))
  def asJson(src: ArticleTag) = Json.stringify(Json.toJson(src))

  def fromMongo(o: DBObject): ArticleTag = {
    new ArticleTag(
        name = o.as[String]("_id")
    )
  }

}


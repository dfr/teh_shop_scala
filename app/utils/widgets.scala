package utils

import models._
import play.api.Logger

object widgets {

    def recentProducts = {
        val products = Product.find(filter=List())
        views.html.widgets.listProducts(products)
    }

    def breadcrumbs(categoryId: String) = {
        val cats : Array[Category] = Category.load(categoryId).map { cat =>
            cat.path.split(',').filterNot(_.isEmpty).map { part => Category.load(part) }.flatten
        }.get
        views.html.widgets.breadcrumbs(cats)
    }
}

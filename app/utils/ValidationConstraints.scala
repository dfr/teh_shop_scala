package utils
import play.api.data.validation._
import models._

object ValidationConstraints {
    val userPasswordCheckConstraint: Constraint[User] = Constraint("constraints.passwordcheck")({ obj =>
        val isValid = if(obj.id == "0") obj.password.nonEmpty else true
        if (isValid) {
            Valid
        } else {
            Invalid(Seq(ValidationError("Password is all numbers")))
        }
    })

    def uniqueUserFieldConstraint(fld: String, obj: User, v: String) : ValidationResult = {
        if(v.isEmpty) {
            Valid
        }
        else {
            val r = User.find(filter=Seq(fld -> v))
            if (r.size == 0) {
                Valid
            }
            else if(r(0).id == obj.id) {
                Valid
            }
            else {
                Invalid(Seq(ValidationError(s"error.${fld}_exists")))
            }
        }
    }

    val uniqueLoginFieldConstraint : Constraint[User] = Constraint("unique.login")({ obj =>
        uniqueUserFieldConstraint("login", obj, obj.login)
    })

    val uniqueEmailFieldConstraint : Constraint[User] = Constraint("unique.login")({ obj =>
        uniqueUserFieldConstraint("email", obj, obj.email)
    })
}

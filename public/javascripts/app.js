

angular.module('shop-app', [])
.controller('LoginCtrl', function($scope, $http) {
    $scope.user = { email: '1111' };
    $scope.login = function(e) {
        e.preventDefault();
        var form = angular.element(e.target);
        $scope.errors = [];
        $http.post(form.attr('action'), $scope.user)
            .success(function (r) { location.href = r.ok })
            .error(function (r) {
                $scope.errors = r.errors;
            });
    };
});



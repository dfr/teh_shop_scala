/* ****** Services ****** */

angular.module('crud-services', ['ngResource'])
.constant('API_URL', '/api/')
.factory('crudEditMethods', function ($location) {
    return function (subj, scope) {
        return {
            save: function(item) {
                item.$save(function(data) {
                    $location.path('/' + subj);
                    scope.$broadcast('eSaved', item);
                }, function(resp) {
                    if(resp.data.errors) scope.errors = resp.data.errors;
                });
            },
            remove: function(item) {
                scope.$broadcast('eRemoved', item);
                if(scope.items) _.remove(scope.items, item);
                item.$delete();
            },
            cancel: function() { $location.path('/' + subj) }
        };
    };
})
.factory('Product', ['$resource', 'API_URL', function($resource,API_URL){
        return $resource(API_URL + 'products/:id', { id: '@id' });
        // "update": {method: "PUT"}
}])
.factory('Category', ['$resource', function($resource){
    return $resource('/api/categories/:id', { id: '@id' });
}])
.factory('Article', ['$resource', function($resource){
    return $resource('/api/articles/:id', { id: '@id' });
}])
.factory('User', ['$resource', function($resource){
    return $resource('/api/users/:id', { id: '@id' });
}])
.factory('AttrIndex', ['$resource', function($resource){
    return $resource('/api/attrindex/:id', { id: '@id' });
}])
.factory('allCategories', ['Category', function(Category){
    var all = null;
    return {
        all: function() {
            if(!all) all = Category.query({});
            return all;
        },
        unset: function() { all = null },
        byId: function(id) {
            if(!all) all = Category.query({});
            return _.find(all, { id: id });
        }
    };
}]);


/* ****** Apps ****** */

var crud_app = angular.module('crud-app', ['ngRoute', 'crud-services','ui.bootstrap','ui.tinymce', 'ngTagsInput'])
.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/:subj/:id/edit', { templateUrl: function(p) { return p.subj + '_form.tpl.html' } })
    .when('/:subj/new', { templateUrl: function(p) { return p.subj + '_form.tpl.html' } })
    .when('/:subj', { templateUrl: function(p) { return p.subj + '_list.tpl.html' }  });
});

// ajax upload service
crud_app.factory('Uploader', function($q, $rootScope) {
    this.upload = function(url, files) {
        var deferred = $q.defer(),
        formdata = new FormData(),
        xhr = new XMLHttpRequest();

        _.each(files, function (file) { formdata.append('file[]', file)  });

        xhr.onreadystatechange = function(r) {
            if (4 === this.readyState) {
                if (xhr.status == 200) {
                    $rootScope.$apply(function() {
                        deferred.resolve(xhr);  
                    });
                } else {
                    $rootScope.$apply(function() {
                        deferred.reject(xhr);  
                    });
                }
            }
        }
        xhr.open("POST", url, true);
        xhr.send(formdata);
        return deferred.promise;
    };
    return this;
});

// ajax apload helper directive
crud_app.directive('fileChange', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('change', function() {
                scope.$apply(function() {
                    scope[attrs['fileChange']](element[0].files);
                })
            })
        },
    }
});

// server-side form validation directive
crud_app.directive('qnValidate', [ function() {
    return {
        link: function(scope, element, attr) {
            var form = element.inheritedData('$formController');
            // no need to validate if form doesn't exists
            if (!form) return;
            // validation model
            var validate = attr.qnValidate;
            // watch validate changes to display validation
            scope.$watch(validate, function(errors) {

                // every server validation should reset others
                // note that this is form level and NOT field level validation
                form.$serverError = { };

                // if errors is undefined or null just set invalid to false and return
                if (!errors) {
                    form.$serverInvalid = false;
                    return;
                }
                // set $serverInvalid to true|false
                form.$serverInvalid = (errors.length > 0);

                // loop through errors
                angular.forEach(errors, function(error, i) {
                    form.$serverError[error.property] = { $invalid: true, message: error.message };
                });
            });
        }
    };
}]);

// CRUD controllers
crud_app.controller('CrudCtrl', ['$scope', function($scope) {
}])
.controller('ProductListCtrl', function($scope, $http, API_URL, Product, crudEditMethods, allCategories) {
    $scope.allCategories = allCategories;
    $scope.categories = allCategories.all();
    $scope.page = { offset: 0, limit: 20, sortType: 'DESC', category: '', keyword: '' };
    $scope.remove = crudEditMethods('products', $scope)['remove'];
    $scope.load_products = function(reload) {
        if(reload) {
            $scope.page.offset = 0;
            $scope.items = [];
        }
        var qresult = Product.query($scope.page);
        qresult.$promise.then(function(items) { $scope.items = ($scope.items || []).concat(items) });
        $http.get(API_URL + 'products/total', { params: _.pick($scope.page,'category','keyword') }).then(function(r) { $scope.total = r.data });
        $scope.page.offset += $scope.page.limit;
    };
    $scope.load_products();

    $scope.$on('eRemoved', function(e, item) {
        $scope.total -= 1;
    });
})
.controller('ProductEditCtrl', function($scope, $http, API_URL, Product, crudEditMethods, $routeParams, allCategories, Uploader) {
    $scope.item = ('id' in $routeParams) ? Product.get({ id: $routeParams.id }) : new Product({});
    angular.extend($scope, crudEditMethods('products', $scope));
    $scope.categories = allCategories.all();
    $scope.searchtypes = window.searchtypes;

    $scope.add_attr = function() {
        $scope.item.attrs = ($scope.item.attrs || []).concat({ name: '', value: '' });
    };
    $scope.remove_attr = function(attr) {
        $scope.item.attrs.splice( _.indexOf($scope.item.attrs, attr), 1 );
    };
    $scope.remove_img = function(f) {
        $http.delete(API_URL + 'products/rmfile/' + f );
        $scope.item.images.splice( _.indexOf($scope.item.images, f), 1 );
    };
    $scope.upload = function(files) {
        var r = Uploader.upload(API_URL + 'products/upload', files);
        r.then(function(xhr) {
            var resp = angular.fromJson(xhr.response);
            $scope.item.images = _.uniq(($scope.item.images || []).concat(resp.files));
        }, function(r) {
            $scope['form'].$serverError['upload'] = { $invalid: true, message: r.response };
        });
    };

    // Typeahead
    $http.get(API_URL + 'productattrs' ).then(function(r) { $scope.typeahead_attrs = r.data });
})
.controller('CategoryEditCtrl', function($scope, Category, allCategories, crudEditMethods, $routeParams) {
    allCategories.all().$promise.then(function(items) {
        $scope.items = items;
        $scope.item = ('id' in $routeParams) ? allCategories.byId($routeParams.id) : new Category({ parent: '' });
        $scope.parents = [{ id: '', title:'-' }].concat(_.reject(items, function(item) {
            var is_me = $routeParams.id && item.id === $routeParams.id;
            var is_my_child = $routeParams.id && $scope.item && item.path.indexOf(',' + $scope.item.id + ',') !== -1;
            return is_me || is_my_child;
        }));
    });
    angular.extend($scope, crudEditMethods('categories', $scope));
    $scope.$on('eSaved', function(e, newitem) {
        //if(! allCategories.byId(newitem.id) ) allCategories.all().push(newitem);
        allCategories.unset();
    });
})
.controller('CategoryListCtrl', function($scope, allCategories, $cacheFactory) {
    $scope.allCategories = allCategories;
    $scope.items = allCategories.all();
    $scope.remove = function(item) {
        _.remove($scope.items, item);
        item.$delete(function() {
            allCategories.unset(); // reload all cats for parent might change
        });
    };
})
.controller('ArticleEditCtrl', function($scope, API_URL, Article, crudEditMethods, $http, $routeParams, $filter, $q) {
    $scope.item = ('id' in $routeParams) ? Article.get({ id: $routeParams.id }) : new Article({});
    angular.extend($scope, crudEditMethods('articles', $scope));
    // for tags input
    $scope.typeahead_tags = [];
    $http.get(API_URL + 'articletags' ).then(function(r) { $scope.typeahead_tags = r.data });
    $scope.load_tags = function(query) {
        return $q.when($filter('filter')($scope.typeahead_tags, {name:query}));
    };
})
.controller('ArticleListCtrl', function($scope, API_URL, Article, crudEditMethods, $http, $cacheFactory) {
    $scope.page = { offset: 0, limit: 20, sortType: 'DESC', tags: '', keyword: '' };
    $scope.remove = crudEditMethods('articles', $scope)['remove'];
    $scope.load_items = function(reload) {
        if(reload) {
            $scope.page.offset = 0;
            $scope.items = [];
        }
        var qresult = Article.query($scope.page);
        qresult.$promise.then(function(items) { $scope.items = ($scope.items || []).concat(items) });
        $http.get(API_URL + 'articles/total', { params: _.pick($scope.page,'tags','keyword') }).then(function(r) { $scope.total = r.data });
        $scope.page.offset += $scope.page.limit;
    };
    $scope.load_items();
    $http.get(API_URL + 'articletags' ).then(function(r) { $scope.articletags = r.data });

    $scope.$on('eRemoved', function(e, item) { $scope.total -= 1 });
})
.controller('UserEditCtrl', function($scope, API_URL, User, crudEditMethods, $http, $filter, $q, $routeParams) {
    $scope.item = ('id' in $routeParams) ? User.get({ id: $routeParams.id }) : new User({ status: "unconfirmed" });
    angular.extend($scope, crudEditMethods('users', $scope));
    // for groups input
    var typeahead_groups = [];
    $http.get(API_URL + 'usergroups' ).then(function(r) { typeahead_groups = r.data });
    $scope.load_groups = function(query) {
        return $q.when($filter('filter')(typeahead_groups, {name:query}));
    };
    $scope.statuses = window.user_statuses;
})
.controller('UserListCtrl', function($scope, API_URL, User, crudEditMethods, $http, $cacheFactory) {
    $scope.page = { offset: 0, limit: 20, sortType: 'DESC', groups: '', keyword: '' };
    $scope.remove = crudEditMethods('users', $scope)['remove'];
    $scope.load_items = function(reload) {
        if(reload) {
            $scope.page.offset = 0;
            $scope.items = [];
        }
        var qresult = User.query($scope.page);
        qresult.$promise.then(function(items) { $scope.items = ($scope.items || []).concat(items) });
        $http.get(API_URL + 'users/total', { params: _.pick($scope.page,'groups','keyword') }).then(function(r) { $scope.total = r.data });
        $scope.page.offset += $scope.page.limit;
    };
    $scope.load_items();
    $scope.$on('eRemoved', function(e, item) { $scope.total -= 1 });
})
.controller('AttrIndexEditCtrl', function($scope, API_URL, AttrIndex, crudEditMethods, $http, $routeParams, $filter, $q) {
    $scope.item = ('id' in $routeParams) ? AttrIndex.get({ id: $routeParams.id }) : new AttrIndex({});
    angular.extend($scope, crudEditMethods('attrindex', $scope));
    $scope.searchtypes = window.searchtypes;
})
.controller('AttrIndexListCtrl', function($scope, API_URL, AttrIndex, crudEditMethods, $http, $cacheFactory) {
    $scope.page = { offset: 0, limit: 20, sortType: 'DESC', keyword: '', searchtype: '' };
    $scope.remove = crudEditMethods('attrindex', $scope)['remove'];
    $scope.searchtypes = window.searchtypes;
    $scope.load_items = function(reload) {
        if(reload) {
            $scope.page.offset = 0;
            $scope.items = [];
        }
        var qresult = AttrIndex.query($scope.page);
        qresult.$promise.then(function(items) { $scope.items = ($scope.items || []).concat(items) });
        $http.get(API_URL + 'attrindex/total', { params: _.pick($scope.page,'keyword','searchtype') }).then(function(r) { $scope.total = r.data });
        $scope.page.offset += $scope.page.limit;
    };
    $scope.load_items();
    $scope.$on('eRemoved', function(e, item) { $scope.total -= 1 });
});

angular.module('admin-app', ['crud-app', 'crud-services'])
.controller('NavBarCtrl', function($scope, $rootScope, $location, Product, Category) {
    var lastPath = '';
    $rootScope.$on('$routeChangeSuccess', function(event, current){
        var pathElements = $location.path().split('/');
        if(pathElements.length > 1) lastPath = pathElements[1];
    });
    $scope.isNavbarActive = function (path) {
        return path === lastPath;
    };
});



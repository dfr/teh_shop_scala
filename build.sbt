name := "tehshop"

version := "1.0-SNAPSHOT"

//scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.mongodb" %% "casbah" % "2.6.3",
//  "org.scalaz" %% "scalaz-core" % "7.0.6",
  "org.imgscalr" % "imgscalr-lib" % "4.2",
  "se.radley" %% "play-plugins-enumeration" % "1.1.0",
  "org.mindrot" % "jbcrypt" % "0.3m"
)     

play.Project.playScalaSettings

templatesImport += "models._"
